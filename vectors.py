import math

import numpy


def vec(x,y,z):
    """return a numpy.array with type numpy.float"""
    return numpy.array([x, y, z], dtype=float)
def vec(point):
    return numpy.array([point[0], point[1], point[2]])
def normalize(v):
    """return a normalized vector in direction v"""
    return v / math.sqrt(numpy.dot(v,v))
def reflect(v, normal):
    # print(2 * (v - (normal * numpy.dot(normal,v))))
    """return v reflected through normal"""
    reflection = v - 2 * (v - normal * numpy.dot(normal, v))
    return reflection