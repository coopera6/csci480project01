import numpy

from vectors import *

class Material():
    def colorAt(self, point, normal, ray, world):
        return vec(1, 1, 1)


class Flat(Material):
    def __init__(self, color=(0.25, 0.5, 1.0)):
        self.color = color
    def colorAt(self, point, normal, ray, world):
        return self.color

class Phong(Material):
    def __init__(self,
                 color=(0.25, 0.5, 1.0),
                 specularColor=(1, 1, 1),
                 ambient=0.2,
                 diffuse=0.5,
                 specular=0.5,
                 shiny=64):
        self.color = vec(color)
        self.specularColor = vec(specularColor)
        self.ambient = ambient
        self.diffuse = diffuse
        self.specular = specular
        self.shiny = shiny

    def colorAt(self, point, normal, ray, world):
        if numpy.dot(normal, ray.vector) > 0:
            '''seems to be shading the inside of spheres'''
            print("inside error")
        color = self.ambient * self.color
        '''is this how the phong shading equation should be used?'''
        for l in world.lights:
            reflection = normalize(reflect(-l.direction, vec(normal)))
            color += self.diffuse * max(0, numpy.dot(l.direction, normal)) * self.color
            color += self.specular * self.specularColor * max(0, numpy.dot(reflection, ray.vector))**self.shiny
        if color[0] > 1:
            color[0] = 1
        if color[1] > 1:
            color[1] = 1
        if color[2] > 1:
            color[2] = 1
        # print(color)
        return color
