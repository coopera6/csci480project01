import math

import numpy

from vectors import *

EPSILON = 1.0e-10
class GeometricObject():
    def hit(self, ray):
        """Returns (t, point, normal, object) if hit
        and t > EPSILON"""
        '''t is distance from original point'''
        return (None, None, None, None)

class Sphere(GeometricObject):
    def __init__(self, point=(0, 0, 0), radius=1,
            material=None):
        if not(material):
            material = Phong()
        self.point = vec(point)
        self.radius = radius
        self.material = material
    def hit(self, ray):
        '''does the ray intersect the sphere'''

        p = vec(ray.point - self.point)
        a = numpy.dot(ray.vector, ray.vector)
        b = 2 * numpy.dot(p, ray.vector)
        c = numpy.dot(p, p) - (self.radius**2)

        d = b * b - (4 * a * c)
        '''print(p, ray.vector, a, b, c)'''
        if d >= 0:
            # print("here")
            d = math.sqrt(d)
            t = (-b - d)/2
            if t > EPSILON:
                intersect = ray.point + (ray.vector * t)
                # print("HIT", ray.vector)
                '''at least one hit, and t will always be closer of the two hits'''
                ''' (distance, point on object, normal to sphere, the sphere) '''''
                return t, intersect, normalize(intersect - self.point), self
        # else:
            # print("no hit")
        return None


