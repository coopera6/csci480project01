from ray import *
class AbstractWorld():
    def colorAt(self, x, y):
        pass
class World(AbstractWorld):
    def __init__(self,
                 objects=None,
                 lights=None,
                 camera=None,
                 maxDepth = 0,
                 neutral = (.5,.5,.5),
                 nsamples = 1,
                 gamma = 1):
        self.objects = objects
        self.lights = lights
        self.camera = camera
        self.maxDepth = maxDepth
        self.neutral = vec(neutral)
        self.nsamples = nsamples
        self.gamma = gamma
    def colorAt(self, x, y):
        # return (.5,.5,.5)
        camray = self.camera.ray(x, y)

        ''' (distance, point on object, normal to sphere, the sphere) '''''
        objecthit = camray.closestHit(self)
        # print("here")
        if objecthit is not None:
            # print("hit")
            return objecthit[3].material.colorAt(objecthit[1], objecthit[2], camray, self)
            '''dist,point,normal,world'''
        else:
            return self.neutral