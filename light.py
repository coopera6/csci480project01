from vectors import *


class Light():
    def __init__(self, direction, color):
        self.direction = normalize(vec(direction))
        self.color = vec(color)
