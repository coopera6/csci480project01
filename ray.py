from vectors import *


class AbstractRay():
    '''return the point at distance along the ray'''
    def pointAt(self, distance):
        return 0,0,0
    '''return (dist,point,normal,obj)'''
    def closestHit(self, world):
        return None


class Ray(AbstractRay):
    def __init__(self, point, vector, depth=0):
        self.point = vec(point)
        self.vector = normalize(vec(vector))

    def pointAt(self, distance):
        return self.point + distance * self.vector

    def closestHit(self, world):
        dist = 1000
        closest = None
        for obj in world.objects:
            objdist = obj.hit(self)
            # print(objdist)
            if objdist is not None:
                if objdist[0] < dist:
                    closest = objdist
        '''for each object, does it hit?
        is its distance smaller that any other?'''
        """return (dist,point,normal,obj)"""
        return closest
